require 'open-uri'
require 'yaml'
require 'json'
require 'uri'

# load any configured options
config = YAML.load_file("_config.yml")

def get_access_token(config)
  # try to set the access_token from the config file
  access_token = config["nikeplus_access_token"]

  # if config file option is empty, get it from the environment variable
  if access_token.nil? or access_token.empty?
    access_token = ENV["NIKEPLUS_ACCESS_TOKEN"]
  end

  access_token
end

def get_app_id(config)
  # try to set the access_token from the config file
  app_id = config["nikeplus_app_id"]

  # if config file option is empty, get it from the environment variable
  if app_id.nil? or app_id.empty?
    app_id = ENV["NIKEPLUS_APP_ID"]
  end

  app_id
end

def validate_configuration(config, args, access_token, app_id)
  validated = true

  if access_token.nil? or access_token.empty?
    puts "Access Token is not set in config or environment variable."
    validated = false
  end

  # if environment variable is empty, need to fail
  if app_id.nil? or app_id.empty?
    puts "App ID is not set in config or environment variable."
    validated = false
  end

  if args.nil? or args.empty?
    puts "Must pass in a valid endpoint to hit."
    validated = false
  end

  validated
end

access_token = get_access_token(config)
app_id = get_app_id(config)

validated = validate_configuration(config, ARGV, access_token, app_id)
endpoint_type = ARGV[0]

if validated
  base_url = "https://api.nike.com".freeze

  ## aggregate sport data
  agg_sport_data_endpoint = "/me/sport"

  ## list activities
  list_activities_endpoint = "/me/sport/activities"

  ## activity_id
  activity_id = ""
  list_activity_endpoint = "/me/sport/activities/#{activity_id}"

  access_token_qs = "?access_token=#{access_token}"
  list_count = 50
  offset = 1
  start_date = ""
  end_date = ""

  case endpoint_type
  when "-list"
    endpoint = list_activities_endpoint

    ARGV.each do |argument|
      if argument.start_with?("-")
        next
      end

      if argument.include?("count=")
        list_count = argument.split('=')[1].to_i
      end

      if argument.include?("offset=")
        offset = argument.split('=')[1].to_i
      end

      if argument.include?("startDate=")
        start_date = argument.split('=')[1].to_s
      end

      if argument.include?("endDate=")
        end_date = argument.split('=')[1].to_s
      end
    end
  when "-activity"
    endpoint = list_activity_endpoint
  when "-aggregate"
    endpoint = agg_sport_data_endpoint
  when "-14days"
    endpoint = list_activities_endpoint

    offset = 1
    list_count = 14
  end

  url = "#{base_url}#{endpoint}#{access_token_qs}"

  if endpoint_type == "-list" or endpoint_type == "-14days"

    if start_date.nil? or start_date.empty?
      url << "&count=#{list_count}"
      url << "&offset=#{offset}"
    else

      date_str = start_date
      if !end_date.nil? and !end_date.empty?
        date_str = end_date
      end

      url << "&startDate=#{start_date}&endDate=#{date_str}"
    end
  end

 # url = "#{base_url}" << "/me/sport/activities?access_token=3d601df976da04b7782ce14060ec04a2&startDate=2013-09-20&endDate=2013-09-20"

  puts url

  json_result = ""

  open(url, 'appid' => app_id, 'Accept' =>'application/json') do |f|
    response = f.read

    json_result = JSON.parse(response)
  end

  if endpoint_type == "-14days"

    stats = Hash.new
    stats_arr = Array.new

    json_result["data"].each do |activity|
      stat = Hash.new

      stat[:date] = activity["startTime"]
      stat[:steps] = activity["metricSummary"]["steps"]

      stats_arr.push(stat)
    end

    stats[:stats] = stats_arr

    puts stats.to_json
  else
    puts json_result
  end

end
