# Nike+ Fuelband API

A quick and dirty Ruby implementation to connect to the Nike+ API and get data on the fuelband.

The implementation is still in its infancy. More to come.

## Configuration

You will need to set the App ID and the Access Token in the config file.

    nikeplus_access_token = "some-key"
    nikeplus_app_id = "fuelband"

If you would prefer, you can also set the values via Environment Variables:

    $ export NIKEPLUS_ACCESS_TOKEN=some-key
    $ export NIKEPLUS_APP_ID=fuelband
    
As far as I know, the App ID should always be set to *fuelband*. The documentation on the Nike+ Developer Site does not state this. 

## Running Client

It is as simple as:

    $ ruby client.rb {endpoint_type} {additional_arguments...}
    
#####Endpoints (endpoint_type)

__List Activities__

* -list

  Additonal Arguments:
  * count=2 (default is 50)  [optional]
  * offset=5 (default is 1)  [optional]
  * startDate=2013-09-20     [optional]
  * endDate=2013-09-21       [optional]
  
If you do not specify an endDate with a startDate, the endDate will use the startDate. If you specify a startDate, the count and offset parameters will be ignored. Please see the Notes About API section below for further explanation.
  
__List Activity__

* -activity

__Aggregate Sport Data__

* -aggregate

__14 Days Of Steps Data__

* -14days

Retrieves the Steps for each of the last 14 days.

### Notes About API

From what I gather, if you want to use the `startDate` and `endDate` parameters, you should not pass along the `offset` or `count` as they will override the `startDate` and `endDate` parameters.

The API documentation does not currently state this, but so far, this is how I have witnessed it function.

## License

Released under the MIT license.
